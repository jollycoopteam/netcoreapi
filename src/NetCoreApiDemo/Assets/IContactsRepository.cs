﻿using NetCoreApiDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApiDemo.Assets
{
    public interface IContactsRepository
    {
		void Add(Contacts item);
		IEnumerable<Contacts> GetAll();
		Contacts Find(string key);
		void Remove(string id);
		void Update(Contacts item);
    }
}
