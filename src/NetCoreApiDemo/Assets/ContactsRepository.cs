﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetCoreApiDemo.Models;

namespace NetCoreApiDemo.Assets
{
	public class ContactsRepository : IContactsRepository
	{
		static List<Contacts> _ctx = new List<Contacts>();
		public void Add(Contacts item)
		{
			_ctx.Add(item);
		}

		public Contacts Find(string key)
		{
			return _ctx.FirstOrDefault(i => i.MobilePhone.Equals(key));
		}

		public IEnumerable<Contacts> GetAll()
		{
			return _ctx;
		}

		public void Remove(string id)
		{
			var item = _ctx.FirstOrDefault(i => i.MobilePhone.Equals(id));
			if (item != null)
			{
				_ctx.Remove(item);
			}
		}

		public void Update(Contacts item)
		{
			var itemToUpdate = _ctx.FirstOrDefault(r => r.MobilePhone == item.MobilePhone);
			if (itemToUpdate != null)
			{
				itemToUpdate.FirstName = item.FirstName;
				itemToUpdate.LastName = item.LastName;
				itemToUpdate.IsFamilyMember = item.IsFamilyMember;
				itemToUpdate.Company = item.Company;
				itemToUpdate.JobTitle = item.JobTitle;
				itemToUpdate.Email = item.Email;
				itemToUpdate.MobilePhone = item.MobilePhone;
				itemToUpdate.DateOfBirth = item.DateOfBirth;
				itemToUpdate.AnniversaryDate = item.AnniversaryDate;
			}
		}
	}
}
