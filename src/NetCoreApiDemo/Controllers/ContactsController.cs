﻿using Microsoft.AspNetCore.Mvc;
using NetCoreApiDemo.Assets;
using NetCoreApiDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApiDemo.Controllers
{
	[Route("api/[controller]")]
    public class ContactsController : Controller
    {
		private IContactsRepository _contacts;
		public ContactsController(IContactsRepository contacts)
		{
			_contacts = contacts;
		}
		[HttpGet]
		public IEnumerable<Contacts> GetAll()
		{
			return _contacts.GetAll();
		}
		[HttpGet("{id}", Name = "GetContacts")]
		public IActionResult GetById(string id)
		{
			var item = _contacts.Find(id);
			if (item == null)
			{
				return NotFound();
			}
			return new ObjectResult(item);
		}
		[HttpPost]
		public IActionResult Create([FromBody] Contacts item)
		{
			if (item == null)
			{
				return BadRequest();
			}
			_contacts.Add(item);
			return CreatedAtRoute("GetContacts", new { Controller = "Contacts", id = item.MobilePhone }, item);
		}
		[HttpPut("{id}")]
		public IActionResult Update(string id, [FromBody] Contacts item)
		{
			if (item == null)
			{
				return BadRequest();
			}
			var contactObj = _contacts.Find(id);
			if (contactObj == null)
			{
				return NotFound();
			}
			_contacts.Update(item);
			return new NoContentResult();
		}
		[HttpDelete("{id}")]
		public void Delete(string id)
		{
			_contacts.Remove(id);
		}
	}
}
